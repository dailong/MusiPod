//
//  Download.swift
//  MusiPod
//
//  Created by Dai Long on 7/13/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation


class Download {

    // MARK: - Properties
    
    var track: Track
    
    var task: URLSessionDownloadTask?
    
    var isDownloading = false
    
    // if host server support resume download
    
    var resumeData: Data?
    
    var progress: Float = 0

    
    // MARK: - Initializer
    
    init(_ track: Track) {
        self.track = track
    }
 
}
