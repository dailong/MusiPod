//
//  TrackSubcriber.swift
//  MusiPod
//
//  Created by Dai Long on 7/8/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation

protocol TrackSubcriber: class {
    var crtTrack: Track? { get set}
}
