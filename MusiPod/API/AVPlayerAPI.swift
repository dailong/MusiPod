//
//  AVPlayerAPI.swift
//  MusiPod
//
//  Created by Dai Long on 7/9/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation
import AVKit
import AVFoundation

class AVPLayerAPI: NSObject {
    
    // MARK: - Properties
    @objc dynamic var playerItem: AVPlayerItem? {
        didSet {
            addObserver()
        }
    }
    @objc dynamic var player: AVPlayer? {
        didSet {
        }
    }
    

    var currentTime: Float {
        get {
            return Float(CMTimeGetSeconds(player!.currentTime()))
        }
        set {
            let newTime = CMTimeMakeWithSeconds(Float64(newValue), 1)
            player?.seek(to: newTime, toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
        }
    }
    
    var duration: Float {
        guard let currentItem = player?.currentItem else { return 0.0 }
        
        return Float(CMTimeGetSeconds(currentItem.duration))
    }
    
    var rate: Float {
        get {
            return player!.rate
        }
        
        set {
            player?.rate = newValue
        }
    }
    
    var playerItemContext = 0

    
    static let shared = AVPLayerAPI()
    
    private override init() {
        super.init()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    func play() {
        if player?.timeControlStatus == AVPlayerTimeControlStatus.paused {
            player!.play()
        }
        else {
            player!.pause()
        }
    }
    
    
}

// MARK: - KVO
extension AVPLayerAPI {
    fileprivate func addObserver() {
        NotificationCenter.default.addObserver(self,
           selector: #selector(finishPlaying),
           name: .AVPlayerItemDidPlayToEndTime,
           object: playerItem)
    }
    
    @objc func finishPlaying(myNotification: NSNotification) {
        let stopPlayerItem: AVPlayerItem = myNotification.object as! AVPlayerItem
        stopPlayerItem.seek(to: kCMTimeZero, completionHandler: nil)
    }


}
