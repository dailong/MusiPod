//
//  QueryService.swift
//  MusiPod
//
//  Created by Dai Long on 7/5/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation


class QueryService {
    typealias JSONDict = [String: Any]
    typealias QueryResult = ([Track]?, String) -> Void

    fileprivate var tracks = [Track]()
    fileprivate var errorMessage = ""
    
    fileprivate let defaultSession = URLSession(configuration: .default)
    fileprivate var dataTask : URLSessionDataTask?
    
    func getSearchResult(_ searchTerm: String, completion: @escaping QueryResult) {
        dataTask?.cancel()
        
        if var urlsComponent = URLComponents(string: "http://j.ginggong.com/jOut.ashx") {
            urlsComponent.query = "code=54db3d4b-518f-4f50-aa34-393147a8aa18&k=\(searchTerm)&h=chiasenhac.com" //&h : host
            guard let urls = urlsComponent.url else {
                print("URLComponent is nil")
                return
            }
            
            dataTask = defaultSession.dataTask(with: urls, completionHandler: { (data: Data?, response: URLResponse? , error: Error?) in
                defer {self.dataTask = nil}
                
                if let error = error {
                    self.errorMessage += error.localizedDescription + "\n"
                }
                
                if let data = data, let response = response as? HTTPURLResponse, response.statusCode == 200 {
                    self.parsingData(for: data)
                }
                
                DispatchQueue.main.async {
                    completion(self.tracks, self.errorMessage)
                }
            })
            dataTask?.resume()
        }
    }
    
    //parsing JSON
    
    enum SerializationError: Error {
        case missing(String)
        case invalid
    }
    
    
    fileprivate func parsingData(for data: Data) {
        tracks.removeAll()
        
        do {
            if let response = try? JSONSerialization.jsonObject(with: data, options: []) as? [JSONDict?] {
                var index = 0;
                for trackJSONDictionary in response! {
                    if let trackJSONDictionary = trackJSONDictionary {
                        guard let Title = trackJSONDictionary["Title"] as? String else {throw SerializationError.missing("Title is missing")}
                        guard let Artist = trackJSONDictionary["Artist"] as? String else {throw SerializationError.missing("Artist is missing")}
                        guard let Avatar = trackJSONDictionary["Avatar"] as? String else {throw SerializationError.missing("Avatar is missing")}
                        guard let Link = trackJSONDictionary["UrlJunDownload"] as? String else {throw SerializationError.missing("UrlJunDownload is missing")}
                        guard let LyricsUrl = trackJSONDictionary["LyricsUrl"] as? String else {throw SerializationError.missing("LyricsUrl is missing")}
                        guard let HostName = trackJSONDictionary["HostName"] as? String else {throw SerializationError.missing("HostName is missing")}
                        
                        //     print(Link)
                        let track = try? Track(Title: Title,
                                               Artist: Artist,
                                               UrlJunDownload: URL(string: Link)!,
                                               HostName: HostName,
                                               Avatar: URL(string: Avatar)!,
                                               LyricsUrl: URL(string: LyricsUrl)!,
                                               index: index)
                        tracks.append(track!)
                        index += 1
                    }
                    else {self.errorMessage += "Problem parsing trackDictionary\n"}
                }
            }
        }
        catch {print(error.localizedDescription)}
    }
    
}
