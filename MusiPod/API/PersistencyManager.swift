//
//  PersistencyManager.swift
//  MusiPod
//
//  Created by Dai Long on 7/4/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation
import UIKit

final class PersistencyManager {
    private var documents: URL {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    }
    
    private var tracks = [Track]()
    
    private enum FileNames{
        static let Tracks = "tracks.json"
    }
    
    init() {
//        let saveURl = documents.appendingPathComponent(FileNames.Tracks)
//        var data = try? Data(contentsOf: saveURl)
//        if data == nil, let bundleURL = Bundle.main.url(forResource: FileNames.Tracks, withExtension: nil) {
//            data = try? Data(contentsOf: bundleURL)
//        }
//
//        if let tracksData = data,
//            let decodeTracks = try? JSONDecoder().decode([Track].self , from: tracksData) {
//            tracks = decodeTracks
//            saveTracks()
//        }
        
        let fileManager = FileManager.default
//        let saveURl = documents.appendingPathComponent(FileNames.Tracks)
        
        do {
            let fileURLarr = try fileManager.contentsOfDirectory(at: documents, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
            
            for fileURL in fileURLarr {
                if (fileURL.lastPathComponent == ".json") {
                    continue
                }
                print("File hihihi: \(fileURL)")
            }
        }
        catch let error {
            print(error.localizedDescription)
        }
        
//        var data = try? Data(contentsOf: saveURl)
//        if data == nil, let bundleURL = Bundle.main.url(forResource: FileNames.Tracks, withExtension: nil) {
//            data = try? Data(contentsOf: bundleURL)
//        }
//
//        if let tracksData = data,
//            let decodeTracks = try? JSONDecoder().decode([Track].self , from: tracksData) {
//            tracks = decodeTracks
//            saveTracks()
//        }
    }
    
    func saveTracks() {
        
        let url = documents.appendingPathComponent(FileNames.Tracks)

        guard let encodeData = try? JSONEncoder().encode(tracks) else {return}
        try? encodeData.write(to: url)
    }
    
    func getTracks() -> [Track] {
        return tracks
    }


//    func addAlbum(_ album: Album, at index: Int) {
//        if (albums.count >= index) {
//            albums.insert(album, at: index)
//        } else {
//            albums.append(album)
//        }
//    }
//
//    func deleteAlbum(at index: Int) {
//        albums.remove(at: index)
//    }
//
    //Notification
//
//    private var cache: URL {
//        return FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0]
//    }
//
//    func saveImage(_ image: UIImage, filename: String) {
//        let url = cache.appendingPathComponent(filename)
//        guard let data = UIImagePNGRepresentation(image) else {return}
//        try? data.write(to: url)
//    }
//
//    func getImage(with filename: String) -> UIImage? {
//        let url = cache.appendingPathComponent(filename)
//        guard let data = try? Data(contentsOf: url) else {
//            return nil
//        }
//        return UIImage(data: data)
//    }
}
