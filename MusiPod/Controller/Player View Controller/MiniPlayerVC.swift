//
//  MiniPlayerVC.swift
//  MusiPod
//
//  Created by Dai Long on 7/8/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import UIKit
import AVKit

protocol MiniPlayerDelegate: class {
    func expand(_ track: Track)
}

class MiniPlayerVC: UIViewController, TrackSubcriber {

    // MARK: - Properties
    weak var delegate: MiniPlayerDelegate?
    var crtTrack: Track?
    let avplayerAPI = AVPLayerAPI.shared
    
    private var timeObserverToken: Any?
    
    private var playerItemContext = 0
    private var playerTimeControlContext = true
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var moreBtn: UIButton!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure(nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
     //   avplayerAPI.player?.removeObserver(self, forKeyPath: #keyPath(AVPlayer.timeControlStatus))
//        if let timeObserverToken = timeObserverToken {
//            avplayerAPI.player?.removeTimeObserver(timeObserverToken)
//            self.timeObserverToken = nil
//        }
    }
    
    // MARK: - Denit
    
    deinit {
        print("MiniPlayer View Controller was deinited")
    }
    
    // MARK: - IBActions
    
    @IBAction func playAction(_ sender: UIButton) {
        guard crtTrack != nil else {
            print("Can't play track!")
            return
        }
        avplayerAPI.play()
    }
    
    @IBAction func moreAction(_ sender: UIButton) {
    }
    
    @IBAction func TapGesture(_ sender: Any) {
        guard let track = crtTrack else { return }
        delegate?.expand(track)
    }
    
}


// MARK: - Internal
extension MiniPlayerVC {
    func configure(_ track: Track?){
        if let track = track {
            titleLabel.text = track.Title
            artistLabel.text = track.Artist
            
            let url = track.UrlJunDownload
            avplayerAPI.playerItem = AVPlayerItem(url: url)
            avplayerAPI.player = AVPlayer(playerItem: avplayerAPI.playerItem)
            
            timeObserverToken = avplayerAPI.player?.addObserver(self,
                                forKeyPath: #keyPath(AVPlayer.timeControlStatus),
                                options: .new,
                                context: nil)
            
            avplayerAPI.player?.play()
        }
        
        else {
            titleLabel.text = nil
            artistLabel.text = nil
        }
        crtTrack = track
    }
}

extension MiniPlayerVC {
    override func observeValue(forKeyPath keyPath: String?,
                               of object: Any?,
                               change: [NSKeyValueChangeKey : Any]?,
                               context: UnsafeMutableRawPointer?) {

        if keyPath == #keyPath(AVPlayer.timeControlStatus) {
            
            let status: AVPlayerTimeControlStatus
            
            if let statusNumber = change?[.newKey] as? NSNumber {
                status = AVPlayerTimeControlStatus(rawValue: statusNumber.intValue)!
            }
            else {
                status = .waitingToPlayAtSpecifiedRate
            }
            
            switch status {
            case .paused:
       //         print("pause on mini")
                playBtn.setImage(#imageLiteral(resourceName: "play-filled-50"), for: .normal)
            case .playing:
         //       print("play on mini")
                playBtn.setImage(#imageLiteral(resourceName: "pause-filled-50"), for: .normal)
            case .waitingToPlayAtSpecifiedRate:
          //      print("wait on mini")
                playBtn.setImage(#imageLiteral(resourceName: "play-filled-50"), for: .normal)
            }
        }
    }
}
