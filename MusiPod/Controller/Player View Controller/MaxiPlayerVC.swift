//
//  MaxiPlayerVC.swift
//  MusiPod
//
//  Created by Dai Long on 7/6/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class MaxiPlayerVC: UIViewController, TrackSubcriber {

    // MARK: - Properties
    
    var crtTrack: Track?
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var avatarImageView: UIImageView!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        avatarImageView.image = UIImage(named: "default-artwork")
    }
    
    // MARK: - Denit
    deinit {
        navigationController?.popViewController(animated: true)
        print("MaxiPlayer View Controller was deinited")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? TrackSubcriber {
            destination.crtTrack = crtTrack
        }
        
    }
 
    // MARK: - IBActions
    @IBAction func dismissPlayer() {
        dismiss(animated: true, completion: nil)
        
    }
}

