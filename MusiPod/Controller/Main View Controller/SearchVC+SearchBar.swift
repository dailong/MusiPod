//
//  SearchVC+SearchBar.swift
//  MusiPod
//
//  Created by Dai Long on 7/5/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation
import UIKit

// MARK: - Searchbar Delegate
extension SearchVC: UISearchBarDelegate {
    
    @objc func dismissKeyboard() {
        searchVC.searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        dismissKeyboard()
        
        guard let searchText = searchBar.text, !searchText.isEmpty else {return}
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        queryService.getSearchResult(searchText) { (results, error) in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            if let results = results {
                if results.count > 0 {
                    self.searchOnlineResult = results
                    self.hasTrack = true
                    self.updateView()
                    self.tableView.reloadData()
                }
                else {
                    self.messageLabel.text = "No Item Found!!"
                }
            }
            if !error.isEmpty {
                print(error.localizedCapitalized)
            }
        }
    }
}


