//
//  LibraryCell.swift
//  MusiPod
//
//  Created by Dai Long on 7/14/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import UIKit

class LibraryCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    static let reuseIdentifier = "LibraryCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
